import { Tickets } from "./tickets.model";

export interface Legs {
    TKTs: Tickets[],
    LegDirection: number,
    ArrStnFull: string,
    DepStnFull: string,
    RouteTransport: string,
    Operator: string,
    DepStn: string,
    DepDate: string,
    LegCo2: string,
    SeatsReservable: string,
    RouteBookingCode: string,
    ArrStn: string,
    ClassesAvailForLeg: string,
    sTrnUltimateDest: string,
    ArrTime: string,
    LegDistance: string,
    SeatsSelected: string,
    RouteName: string,
    NumberOfStops: string,
    sTrnStartingPoint: string,
    DepTime: string
}