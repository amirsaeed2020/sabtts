import { Component, OnInit } from '@angular/core';
import { ApiService } from './shared/services/api.service';
import { Observable, Subject } from 'rxjs';
import { Results } from './shared/model/results.model';
import { catchError, map, shareReplay, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public results$: Observable<Results[]>;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.results$ = this.apiService
      .get()
      .pipe(
        map(results => results)
      )
  }

  toggle = () => {

  }

}
