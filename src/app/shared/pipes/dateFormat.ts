import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe, } from '@angular/common'

@Pipe({
    name: 'dateFormat'
})
export class DateFormat implements PipeTransform {
    constructor(public datepipe: DatePipe) { }
    transform(value: any, args: string[]): any {
        if (value) {
            let date = new Date();
            return this.datepipe.transform(date, 'yyyy-MM-dd');
        }
    }
}