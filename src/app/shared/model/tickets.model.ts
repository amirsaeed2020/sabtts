export interface Tickets {
    AdtPrice: string,
    CanTicketOnDeparture: string,
    ChdPrice: string,
    MaxAdtPerTck: string,
    MaxChdPerTck: string,
    MaxPaxPerTck: string,
    MinAdtPerTck: string,
    MinChdPerTck: string,
    MinPaxPerTck: string,
    Operator: string,
    ProfileShouldNotBook: string,
    SingleOrReturn: string,
    TicketCode: string,
    TicketDescription: string,
    TravelCardDiscAdt: string,
    TravelCardDiscChd: string,
    TravelCardsApplied: string,
    TravelTimes: string,
    ValidFrom: string,
    ValidTo: string
}