import { Request, Response } from 'express';
import { TICKETS } from './data';

export function getAllTickets(req: Request, res: Response) {

  console.log("Retrieving ticket data ...");

  setTimeout(() => {

    res.status(200).json({ payload: Object.values(TICKETS) });

  }, 1000);
}