import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { Results } from '../model/results.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private httpClient: HttpClient) { }

  get = (): Observable<Results[]> => {
    return this.httpClient.get<Results[]>("assets/data/results.json");
  }
}
